package com.example.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Farmer;
import com.example.util.DataBaseUtil;

public class FarmerRepository {
    private DataBaseUtil dataBaseUtil;
    private static final String SQL_FIND_ALL = "SELECT * FROM farmers";
    private static final String SQL_FIND_BY_ID = "SELECT * FROM farmers WHERE EMAIL = (?) ";
    private static final String SQL_INSERT = "INSERT INTO farmers (email , password ) values (?,?) ";
    private static final String SQL_UPDATE = "UPDATE farmers set farmer_name = ?,farmer_phone_number=?,farmer_city=?,farmer_state = ?,farmer_gender=?,farmer_address=?,farmer_about=?,farmer_password=?,farmer_pincode=? WHERE farmer_email = ?";
    private static final String SQL_DELETE = "DELETE farmers WHERE farmer_email = ?";

    public FarmerRepository(){
        this.dataBaseUtil =  new DataBaseUtil();
    }
  
    //get all farmers id,name ,email,city
    public List<Farmer> getAllFarmers(){
        List<Farmer> farmers = new ArrayList<>();
        try{
            PreparedStatement statement = dataBaseUtil.getConnection().prepareStatement(SQL_FIND_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Farmer farmer = new Farmer();
                farmer.setFarmerId(Integer.parseInt(resultSet.getString(1)));
                farmer.setFarmerName(resultSet.getString(2));
                farmer.setFarmerEmailId(resultSet.getString(3));
                farmer.setFarmerCity(resultSet.getString(5));
                farmers.add(farmer);
            }
        }catch(SQLException e){
            System.out.println("sorry cant get the all farmers .");
        }finally{
            dataBaseUtil.close();
        }
        return farmers;
    }

    //get farmers by email
    public List<Farmer> getAllFarmersByEmail(String email){
        List<Farmer> farmers = new ArrayList<>();
        try{
            PreparedStatement statement = dataBaseUtil.getConnection().prepareStatement(SQL_FIND_BY_ID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Farmer farmer = new Farmer();
                farmer.setFarmerId(Integer.parseInt(resultSet.getString(1)));
                farmer.setFarmerName(resultSet.getString(2));
                farmer.setFarmerEmailId(resultSet.getString(3));
                farmer.setFarmerCity(resultSet.getString(5));
                farmers.add(farmer);
            }
        }catch(SQLException e){
            System.out.println("sorry cant get the all farmers .");
        }finally{
            dataBaseUtil.close();
        }
        return farmers;
    }

    //insert farmers
    public void insertFarmer(Farmer farmer){
        try{
            PreparedStatement statement = dataBaseUtil.getConnection().prepareStatement(SQL_INSERT);
            statement.setString(1,farmer.getFarmerName());
            statement.setString(2,farmer.getFarmerEmailId());
            statement.executeUpdate();
        }catch(SQLException e){
            System.out.println("sorry cant insert the  farmers .");
        }finally{
            dataBaseUtil.close();
        }
    }

    //update farmers
    public void updateFarmer(Farmer farmer){
        try{
            PreparedStatement statement = dataBaseUtil.getConnection().prepareStatement(SQL_UPDATE);
            statement.setString(1, farmer.getFarmerName());
            statement.setLong(2,farmer.getFarmerPhoneNumber());
            statement.setString(3, farmer.getFarmerCity());
            statement.setString(4, farmer.getFarmerState());
            statement.setString(5, farmer.getFarmerGender());
            statement.setString(6, farmer.getFarmerAddress());
            statement.setString(7, farmer.getFarmerAbout());
            statement.setString(8, farmer.getFarmerPassword());
            statement.setInt(9, farmer.getFarmerPincode());
            statement.setString(9, farmer.getFarmerEmailId());
            statement.executeUpdate();
        }catch(SQLException e){
            System.out.println("Sorry cant update farmer ");
        }finally{
            dataBaseUtil.close();
        }
    }

    //delete farmer account
    public void deleteFarmer(Farmer farmer){
        try {
            PreparedStatement statement = dataBaseUtil.getConnection().prepareStatement(SQL_DELETE);
            statement.setString(1, farmer.getFarmerEmailId());  
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println("Cant delete");
        }finally{
            dataBaseUtil.close();
        }
    }

    

}
