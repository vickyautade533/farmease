package com.example.controller;

import com.example.views.FarmerView;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FarmerController extends Application {
    private Stage primaryStage;
    private FarmerView farmerView;
    private String viewType;

    public FarmerController(String viewType) {
        this.viewType = viewType;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        farmerView = new FarmerView(viewType);
        switch (viewType) {
            case "home":
                showHomePage();
                break;
            case "signup":
                showSignUpView();
                break;
            case "login":
                showLoginView();
                break;
        }
        this.primaryStage.setTitle("FarmEase");
        this.primaryStage.show();
    }

    private void showLoginView() {
        Group loginView = farmerView.getFarmerLoginView();
        Scene scene = new Scene(loginView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

    private void showSignUpView() {
        Group signUpView = farmerView.getFarmerSignUpView();
        Scene scene = new Scene(signUpView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

    private void showHomePage() {
        Group homePageView = farmerView.getHomePage();
        Scene scene = new Scene(homePageView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

}
