package com.example.controller;

import com.example.views.AdminView;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AdminController extends Application {
    private Stage primaryStage;
    private AdminView adminView;
    private String viewType;

    public AdminController(String type) {
        this.viewType = viewType;
    }

    // start implementation
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        adminView = new AdminView(viewType);
        switch (viewType) {
            case "home":
                showHomePage();
                break;
            case "signup":
                showSignUpView();
                break;
            case "login":
                showLoginView();
                break;
        }
        this.primaryStage.setTitle("FarmEase");
        this.primaryStage.show();
    }

    private void showLoginView() {
        Group loginView = adminView.getLoginPage();
        Scene scene = new Scene(loginView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

    private void showSignUpView() {
        Group signUpView = adminView.getSignUpPage();
        Scene scene = new Scene(signUpView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

    private void showHomePage() {
        Group homePageView = adminView.getHomePage();
        Scene scene = new Scene(homePageView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

}
