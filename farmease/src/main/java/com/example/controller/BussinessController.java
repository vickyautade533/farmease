package com.example.controller;

import com.example.views.BussinessView;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class BussinessController extends Application {
    private Stage primaryStage;
    private BussinessView bussinessView;
    private String viewtype;

    public BussinessController(String type) {
        this.viewtype = viewtype;
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage =primaryStage;
        bussinessView = new BussinessView(viewtype);
        switch (viewtype) {
            case "home":
            showHomePage();
            break;
        case "signup":
            showSignUpView();
            break;
        case "login":
            showLoginView();
            break;
        }
        this.primaryStage.setTitle("FarmEase");
        this.primaryStage.show();
    }

     private void showLoginView() {
        Group loginView = bussinessView.getLoginPage();
        Scene scene = new Scene(loginView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

    private void showSignUpView() {
        Group signUpView = bussinessView.getSignUpPage();
        Scene scene = new Scene(signUpView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }

    private void showHomePage() {
        Group homePageView = bussinessView.getHomePage();
        Scene scene = new Scene(homePageView, 1800, 1000);
        this.primaryStage.setScene(scene);
    }


}
