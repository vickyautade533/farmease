package com.example.controller;

import com.example.views.Index;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

public class LoginSignupController extends Application {
    private ComboBox<String> loginComboBox;
    private ComboBox<String> signComboBox;
    private Index indexView;
    private Stage primaryStage;
    FarmerController farmerController;
    AdminController adminController;
    BussinessController manufacturerController;

    //overide the start method
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        indexView = new Index();
        loginComboBox = indexView.getLoginComboBox();
        signComboBox = indexView.getSignupComboBox();

        loginComboBox.setOnAction(event -> {
            String selectedLoginType = loginComboBox.getValue();
            handleLogin(selectedLoginType,"login");
        });

        signComboBox.setOnAction(event -> {
            String selectedSignUpType = signComboBox.getValue();
            handleSignUp(selectedSignUpType,"signup");
        });

       

        Scene scene = new Scene(indexView.getContent(), 1800, 1000);
        this.primaryStage.setScene(scene);
        this.primaryStage.setTitle("FarmEase Login");
        this.primaryStage.show();
    }


    // handling LoginPages
    public void handleLogin(String loginType,String type) {
        switch (loginType) {
            case "Admin":
                launchAdminController(type);
                break;
            case "Farmer":
                launchFarmerController(type);
                break;
            case "Business":
                launchManufacturerController(type);
                break;
        }
    }

    public void handleSignUp(String signUpType,String type) {
        switch (signUpType) {
            case "Farmer":
                launchFarmerController(type);
                break;
            case "Admin":
                launchFarmerController(type);
                break;
            case "Business":
                launchFarmerController(type);
                break;
        }
    }

    




    // launching the launchfarmerController from loginController
    private void launchFarmerController(String type) {
        farmerController = new FarmerController(type);
        System.out.println("demo");
        try {
            farmerController.start(this.primaryStage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // launching the launchAdminController from loginController
    private void launchAdminController(String type) {
        adminController = new AdminController(type);
        try {
            adminController.start(this.primaryStage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // launching the launchManufacturerController from loginController
    private void launchManufacturerController(String type) {
        manufacturerController = new BussinessController(type);
        try {
            manufacturerController.start(this.primaryStage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
