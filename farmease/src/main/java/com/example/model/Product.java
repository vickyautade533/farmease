package com.example.model;

import javafx.scene.control.Button;

public class Product {
    private String productUrl;
    private String productTitle;
    private String productDescription;
    private double productPrice;
    private int productQuantity;
    private Button addCart;

    public Button getAddCart() {
        return addCart;
    }
    public void setAddCart(Button addCart) {
        this.addCart = addCart;
    }
    public String getProductUrl() {
        return productUrl;
    }
    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }
    public String getProductTitle() {
        return productTitle;
    }
    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }
    public String getProductDescription() {
        return productDescription;
    }
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
    public double getProductPrice() {
        return productPrice;
    }
    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }
    public int getProductQuantity() {
        return productQuantity;
    }
    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }
    
}
