package com.example.model;

public class Weather {
    private String location;
    private String  country;

    private String dayday1;
    private double minTempday1;
    private double maxTempday1;
    private double windSpeedday1;
    private String conditionday1;

    private String dayday2;
    private double minTempday2;
    private double maxTempday2;
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getDayday1() {
        return dayday1;
    }
    public void setDayday1(String dayday1) {
        this.dayday1 = dayday1;
    }
    public double getMinTempday1() {
        return minTempday1;
    }
    public void setMinTempday1(double minTempday1) {
        this.minTempday1 = minTempday1;
    }
    public double getMaxTempday1() {
        return maxTempday1;
    }
    public void setMaxTempday1(double maxTempday1) {
        this.maxTempday1 = maxTempday1;
    }
    public double getWindSpeedday1() {
        return windSpeedday1;
    }
    public void setWindSpeedday1(double windSpeedday1) {
        this.windSpeedday1 = windSpeedday1;
    }
    public String getConditionday1() {
        return conditionday1;
    }
    public void setConditionday1(String conditionday1) {
        this.conditionday1 = conditionday1;
    }
    public String getDayday2() {
        return dayday2;
    }
    public void setDayday2(String dayday2) {
        this.dayday2 = dayday2;
    }
    public double getMinTempday2() {
        return minTempday2;
    }
    public void setMinTempday2(double minTempday2) {
        this.minTempday2 = minTempday2;
    }
    public double getMaxTempday2() {
        return maxTempday2;
    }
    public void setMaxTempday2(double maxTempday2) {
        this.maxTempday2 = maxTempday2;
    }
    public double getWindSpeedday2() {
        return windSpeedday2;
    }
    public void setWindSpeedday2(double windSpeedday2) {
        this.windSpeedday2 = windSpeedday2;
    }
    public String getConditionday2() {
        return conditionday2;
    }
    public void setConditionday2(String conditionday2) {
        this.conditionday2 = conditionday2;
    }
    public String getDayday3() {
        return dayday3;
    }
    public void setDayday3(String dayday3) {
        this.dayday3 = dayday3;
    }
    public double getMinTempday3() {
        return minTempday3;
    }
    public void setMinTempday3(double minTempday3) {
        this.minTempday3 = minTempday3;
    }
    public double getMaxTempday3() {
        return maxTempday3;
    }
    public void setMaxTempday3(double maxTempday3) {
        this.maxTempday3 = maxTempday3;
    }
    public double getWindSpeedday3() {
        return windSpeedday3;
    }
    public void setWindSpeedday3(double windSpeedday3) {
        this.windSpeedday3 = windSpeedday3;
    }
    public String getConditionday3() {
        return conditionday3;
    }
    public void setConditionday3(String conditionday3) {
        this.conditionday3 = conditionday3;
    }
    public String getDayday4() {
        return dayday4;
    }
    public void setDayday4(String dayday4) {
        this.dayday4 = dayday4;
    }
    public double getMinTempday4() {
        return minTempday4;
    }
    public void setMinTempday4(double minTempday4) {
        this.minTempday4 = minTempday4;
    }
    public double getMaxTempday4() {
        return maxTempday4;
    }
    public void setMaxTempday4(double maxTempday4) {
        this.maxTempday4 = maxTempday4;
    }
    public double getWindSpeedday4() {
        return windSpeedday4;
    }
    public void setWindSpeedday4(double windSpeedday4) {
        this.windSpeedday4 = windSpeedday4;
    }
    public String getConditionday4() {
        return conditionday4;
    }
    public void setConditionday4(String conditionday4) {
        this.conditionday4 = conditionday4;
    }
    public String getDayday5() {
        return dayday5;
    }
    public void setDayday5(String dayday5) {
        this.dayday5 = dayday5;
    }
    public double getMinTempday5() {
        return minTempday5;
    }
    public void setMinTempday5(double minTempday5) {
        this.minTempday5 = minTempday5;
    }
    public double getMaxTempday5() {
        return maxTempday5;
    }
    public void setMaxTempday5(double maxTempday5) {
        this.maxTempday5 = maxTempday5;
    }
    public double getWindSpeedday5() {
        return windSpeedday5;
    }
    public void setWindSpeedday5(double windSpeedday5) {
        this.windSpeedday5 = windSpeedday5;
    }
    public String getConditionday5() {
        return conditionday5;
    }
    public void setConditionday5(String conditionday5) {
        this.conditionday5 = conditionday5;
    }
    private double windSpeedday2;
    private String conditionday2;

    private String dayday3;
    private double minTempday3;
    private double maxTempday3;
    private double windSpeedday3;
    private String conditionday3;

    private String dayday4;
    private double minTempday4;
    private double maxTempday4;
    private double windSpeedday4;
    private String conditionday4;

    private String dayday5;
    private double minTempday5;
    private double maxTempday5;
    private double windSpeedday5;
    private String conditionday5;
     
}
