package com.example.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import com.example.model.News;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.json.Json;

public class ApiBinding {
            public List<News> getNews(String newsApiUrl) throws Exception{
                String API_URL = newsApiUrl;
                URL url = new URL(API_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                String singleLineContent;
                StringBuilder totalContent = new StringBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((singleLineContent = br.readLine()) != null) {
                    totalContent.append(singleLineContent);
                }
                return parseNewsData(totalContent.toString());
            }

            

            private List<News> parseNewsData(String reponse) throws Exception{
                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(reponse);
                JsonNode articles = root.path("articles");
                List<News> NewsItems = new ArrayList<News>();
                for (JsonNode article : articles) {
                    News item = new News();
                    
                    NewsItems.add(item);
                }
                return NewsItems;
            }
}
