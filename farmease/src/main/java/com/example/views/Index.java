package com.example.views;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;

public class Index {

    private VBox content;
    private StackPane sp;
    private ComboBox<String> login;
    private ComboBox<String> signup;

    public Index() {
        initializeView();
    }
    

    private void initializeView() {
        sp = new StackPane();
        Image img = new Image("farmForm/back2.jpg");
        ImageView iv = new ImageView(img);
        iv.setFitHeight(1000);
        iv.setFitWidth(1800);
        iv.setPreserveRatio(false);

        VBox blurredVBox = new VBox(10);
        blurredVBox.setPrefWidth(1700);
        blurredVBox.setPrefHeight(900);
        blurredVBox.setAlignment(Pos.CENTER_LEFT);
        blurredVBox.setPadding(new Insets(20));
        blurredVBox.setStyle(
                "-fx-background-color: rgba(255, 255, 255, 0.3); -fx-background-radius:25; -fx-border-color:black; ");
        blurredVBox.setEffect(new GaussianBlur(10));

        // --------------------------------------------------------------------------------------------------------------

        Image logo = new Image("farmForm/logo.gif");
        ImageView liv = new ImageView(logo);
        liv.setFitWidth(70);
        liv.setFitHeight(70);

        Label title = new Label("FarmEase");
        title.setFont(new Font("IMPACT", 50));
        title.setTextFill(Color.BLACK);

        HBox navbarLeft = new HBox(40, liv, title); // navbar
        navbarLeft.setPrefHeight(150);
        navbarLeft.setStyle("-fx-padding:40");

        // -----------------------------------------------------------------------------------------------------------------

        login = new ComboBox<>();
        login.getItems().addAll("Farmer", "Admin", "Business");
        login.setPromptText("Login as");
        login.setStyle(
                "-fx-background-color:transparent; -fx-font-family:IMPACT; -fx-font-size:25; -fx-text-fill:black; ");

        signup = new ComboBox<>();
        signup.getItems().addAll("Farmer", "Admin", "Business");
        signup.setPromptText("Signup as");
        signup.setStyle("-fx-background-color:transparent; -fx-font-family:IMPACT; -fx-font-size:25;");

        signup.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> listView) {
                ListCell<String> cell = new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                        } else {
                            setText(item);
                            setAlignment(Pos.CENTER);
                            setStyle(
                                    "-fx-background-color: rgba(253, 255, 226, 0.7); " +
                                            "-fx-text-fill: black; " +
                                            "-fx-background-radius: 0; " +
                                            "-fx-border-color: black; " +
                                            "-fx-border-width: 1; " +
                                            "-fx-border-radius: 0; " +
                                            "-fx-padding: 5;" +
                                            "-fx-margin: 0;");
                        }
                    }
                };
                return cell;
            }
        });

        login.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> listView) {
                ListCell<String> cell = new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                        } else {
                            setText(item);
                            setAlignment(Pos.CENTER);
                            setStyle(
                                    "-fx-background-color: rgba(253, 255, 226, 0.7); " +
                                            "-fx-text-fill: black; " +
                                            "-fx-background-radius: 0; " +
                                            "-fx-border-color: black; " +
                                            "-fx-border-width: 1; " +
                                            "-fx-border-radius: 0; " +
                                            "-fx-padding: 5;" +
                                            "-fx-margin: 0;");
                        }
                    }
                };
                return cell;
            }
        });

        HBox navbarRight = new HBox(80, login, signup);
        navbarRight.setPrefHeight(150);
        navbarRight.setStyle("-fx-padding:60");

        // ----------------------------------------------------------------------------------------------------------

        HBox navbar = new HBox(650, navbarLeft, navbarRight); // navbar
        navbar.setStyle(
                "-fx-border-width:5; -fx-border-radius:25; -fx-background-color:rgb(253, 255, 226,0.3); -fx-background-radius:25; -fx-border-color:rgb(200, 207, 160,0.7)");
        navbar.setAlignment(Pos.CENTER);

        // ----------------------------------------------------------------------------------------------------------------

        Label story = new Label("Our story");
        story.setFont(new Font("IMPAct", 35));
        story.setStyle("-fx-text-fill:black");

        Label info = new Label(
                " Agriculture is the backbone of our world, providing essential resources and\n sustaining communities. At farmEase, we are passionate about supporting farmers and\n agricultural professionals by offering innovative tools, comprehensive resources, and\n a vibrant community. Our mission is to empower agriculture through knowledge, technology,\n and collaboration.");
        info.setFont(new Font("IMPAct", 25));
        info.setStyle("-fx-padding:0 0 0 50; -fx-text-fill:black; -fx-line-spacing:20 ");

        VBox desc = new VBox(40, story, info);

        // --------------------------------------------------------------------------------------------------------------

        Image flogo = new Image("farmForm/logo.gif");
        ImageView fiv = new ImageView(flogo);
        fiv.setFitHeight(50);
        fiv.setFitWidth(50);
        fiv.setStyle("-fx-background-radius:50");

        Label ftext = new Label("FarmEase");
        ftext.setFont(new Font("IMPACT", 30));
        ftext.setStyle("-fx-text-fill:black");

        HBox finfo = new HBox(20, fiv, ftext);
        finfo.setAlignment(Pos.CENTER);

        Label copyright = new Label("                @ 2024 FarmEase");
        copyright.setFont(new Font("IMPACT", 20));
        copyright.setStyle("-fx-text-fill:black");

        VBox footer = new VBox(20, finfo, copyright);
        footer.setStyle("-fx-padding:10 0 0 1400");

        // -----------------------------------------------------------------------------------------------------------------

        content = new VBox(100, navbar, desc, footer); 
        content.setStyle("-fx-padding:40");

        StackPane stackPane = new StackPane(blurredVBox, content);
        StackPane.setAlignment(title, Pos.TOP_LEFT);
        StackPane.setMargin(title, new Insets(30, 0, 0, 30)); // Adjust margins as needed

        HBox hbox = new HBox(10, stackPane);
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.setPadding(new Insets(50));

        sp.getChildren().addAll(iv, hbox);
    }


    public StackPane getContent() {
        return sp;
    }

    public ComboBox<String> getLoginComboBox() {
        return login;
    }

    public ComboBox<String> getSignupComboBox() {
        return signup;
    }

}
