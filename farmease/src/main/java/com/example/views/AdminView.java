package com.example.views;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class AdminView {

    private Group adminLoginView;
    private Group adminSignUpView;
    private Group adminHomePageView;

    public AdminView(String viewType) {
        if (viewType.equals("login")) {
            LoginPage();
        }
        if (viewType.equals("signup")) {
            SignUpPage();
        }
        if (viewType.equals("home")) {
            HomePage();
        }
    }

    // code for admin homepage
    public void HomePage() {

    }

    // code for admin loginpage
    public void LoginPage() {
        adminLoginView = new Group();
        Image img = new Image("farmForm/back8.jpg");
        ImageView iv = new ImageView(img);
        iv.setFitWidth(920);
        iv.setFitHeight(1000);

        HBox left = new HBox(iv);
        left.setAlignment(Pos.BOTTOM_CENTER);

        // ------------------------------------------------------------------------------------------------------------------

        Label lb1 = new Label("Welcome Back !!");
        lb1.setFont(new Font("IMPACT", 50));

        Image mail = new Image("icons/mail.gif");
        ImageView m = new ImageView(mail);
        m.setFitHeight(30);
        m.setFitWidth(30);

        TextField email = new TextField();
        email.setPromptText("Email");
        email.setFont(new Font(30));
        email.setStyle("-fx-background-color: transparent;");

        HBox mailBox = new HBox(10, m, email);
        mailBox.setStyle("-fx-background-color: transparent;  -fx-border-color: grey; -fx-border-width: 0 0 2 0;");
        mailBox.setAlignment(Pos.CENTER_LEFT);

        // ---------------------------------------------------------------------------------------------------------------------

        Image password = new Image("icons/lock.gif");
        ImageView p = new ImageView(password);
        p.setFitHeight(30);
        p.setFitWidth(30);

        PasswordField pass = new PasswordField();
        pass.setPromptText("Password");
        pass.setFont(new Font(30));
        pass.setStyle("-fx-background-color: transparent;");

        HBox passBox = new HBox(10, p, pass);
        passBox.setStyle("-fx-background-color: transparent;  -fx-border-color: grey; -fx-border-width: 0 0 2 0;");
        passBox.setAlignment(Pos.CENTER_LEFT);

        // ------------------------------------------------------------------------------------------------

        Button btn = new Button("Login");
        btn.setFont(new Font(30));
        btn.setStyle(
                "-fx-background-color:black; -fx-border-color: black; -fx-border-width: 0; -fx-text-fill: white; -fx-border-radius: 100; -fx-background-radius: 100");

        btn.setOnMouseEntered(event -> {
            btn.setScaleX(1.1);
            btn.setScaleY(1.1);

        });

        btn.setOnMouseExited(event -> {
            btn.setScaleX(1);
            btn.setScaleY(1);

        });

        btn.setOnAction(event -> {

            if (email.getText().isEmpty() || pass.getText().isEmpty()) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Enter required data to login");
                alert.setX(800);
                alert.setY(50);
                alert.showAndWait();
            }

        });
        // --------------------------------------------------------

        Label lb4 = new Label("Don't have an account?");
        lb4.setFont(new Font(25));

        Button lg = new Button("Sign Up");
        lg.setFont(new Font(25));
        btn.setPrefWidth(170);
        lg.setStyle(
                "-fx-background-color: transparent; -fx-text-fill: orange; -fx-border-color: none; -fx-padding: 0 0 1 0;");

        lg.setOnMouseEntered(event -> {
            lg.setStyle(
                    "-fx-text-fill:red; -fx-background-color: transparent; -fx-border-color: none; -fx-padding: 0 0 1 0;");

        });

        lg.setOnMouseExited(event -> {

            lg.setStyle(
                    "-fx-text-fill:orange; -fx-background-color: transparent; -fx-border-color: none; -fx-padding: 0 0 1 0;");

        });

        HBox lhb = new HBox(30, lb4, lg);
        lhb.setStyle("-fx-padding:0");
        lhb.setAlignment(Pos.CENTER);

        // -----------------------------------------------------------------------------------------------------------

        VBox right = new VBox(40, lb1, mailBox, passBox, btn, lhb);
        right.setAlignment(Pos.CENTER);
        HBox hb = new HBox(right);
        hb.setPrefWidth(600);
        hb.setAlignment(Pos.CENTER);

        VBox vb = new VBox(hb);
        vb.setPrefWidth(1600);
        vb.setPrefHeight(600);
        vb.setAlignment(Pos.CENTER);
        vb.setStyle("-fx-padding: 0 0 0 0; ");

        HBox hbMain = new HBox(vb, left);
        hbMain.setPrefSize(1800, 1000);
        hbMain.setStyle("-fx-padding: 0 60 0 60;");
        hbMain.setAlignment(Pos.CENTER);
        adminLoginView.getChildren().addAll(hbMain);
    }

    // code for admin signup page
    public void SignUpPage() {

        adminSignUpView = new Group();

        Image img = new Image("farmForm/back7.jpg");
        ImageView iv = new ImageView(img);
        iv.setFitWidth(900);
        iv.setFitHeight(900);

        VBox left = new VBox(iv);
        left.setPrefWidth(700);
        left.setPrefHeight(700);
        left.setAlignment(Pos.CENTER);

        // ------------------------------------------------------------------------------------------

        Label heading = new Label("For Admin");
        heading.setFont(new Font("IMPACT", 40));
        heading.setAlignment(Pos.CENTER);

        Label lb1 = new Label("Save your account now");
        lb1.setFont(new Font("IMPACT", 50));

        Label lb2 = new Label("Rooted in Tradition, Growing for Tomorrow");
        lb2.setFont(new Font(20));
        lb2.setStyle("-fx-font-weight:500");

        VBox vb1 = new VBox(30, heading, lb1, lb2);
        vb1.setAlignment(Pos.CENTER);

        // -------------------------------------------------

        Image user = new Image("icons/user.gif");
        ImageView usr = new ImageView(user);
        usr.setFitHeight(30);
        usr.setFitWidth(30);

        TextField name = new TextField();
        name.setPromptText("Name or nickname");
        name.setFont(new Font(30));
        name.setStyle("-fx-background-color: transparent;");

        HBox userBox = new HBox(10, usr, name);
        userBox.setStyle("-fx-background-color: transparent;  -fx-border-color: grey; -fx-border-width: 0 0 2 0;");
        userBox.setAlignment(Pos.CENTER_LEFT);

        // ---------------------------------------------------

        Image mail = new Image("icons/mail.gif");
        ImageView m = new ImageView(mail);
        m.setFitHeight(30);
        m.setFitWidth(30);

        TextField email = new TextField();
        email.setPromptText("Email");
        email.setFont(new Font(30));
        email.setStyle("-fx-background-color: transparent;");

        HBox mailBox = new HBox(10, m, email);
        mailBox.setStyle("-fx-background-color: transparent;  -fx-border-color: grey; -fx-border-width: 0 0 2 0;");
        mailBox.setAlignment(Pos.CENTER_LEFT);

        // ----------------------------------------------------------------------------------------------

        Image password = new Image("icons/lock.gif");
        ImageView p = new ImageView(password);
        p.setFitHeight(30);
        p.setFitWidth(30);

        PasswordField pass = new PasswordField();
        pass.setPromptText("Password");
        pass.setFont(new Font(30));
        pass.setStyle("-fx-background-color: transparent;");

        HBox passBox = new HBox(10, p, pass);
        passBox.setStyle("-fx-background-color: transparent;  -fx-border-color: grey; -fx-border-width: 0 0 2 0;");
        passBox.setAlignment(Pos.CENTER_LEFT);

        // ---------------------------------------------------------------------------------------------

        Image repassd = new Image("icons/lock2.gif");
        ImageView rp = new ImageView(repassd);
        rp.setFitHeight(35);
        rp.setFitWidth(35);

        PasswordField rePass = new PasswordField();
        rePass.setPromptText("Re-enter Password");
        rePass.setFont(new Font(30));
        rePass.setStyle("-fx-background-color: transparent;");

        HBox rePassBox = new HBox(10, rp, rePass);
        rePassBox.setStyle(
                "-fx-background-color: transparent; -fx-padding: 0 0 0 0;  -fx-border-color: grey; -fx-border-width: 0 0 2 0;");
        rePassBox.setAlignment(Pos.CENTER_LEFT);

        // -----------------------------------------------------------------------------

        VBox vb2 = new VBox(30, userBox, mailBox, passBox, rePassBox);
        vb2.setStyle("-fx-padding:0");

        // ------------------------------------------------------

        Button btn = new Button("Sign up ");
        btn.setFont(new Font(30));
        btn.setStyle(
                "-fx-background-color:black; -fx-border-color: black; -fx-border-width: 0; -fx-text-fill: white; -fx-border-radius: 100; -fx-background-radius: 100");

        btn.setOnMouseEntered(event -> {
            btn.setScaleX(1.1);
            btn.setScaleY(1.1);

        });

        btn.setOnMouseExited(event -> {
            btn.setScaleX(1);
            btn.setScaleY(1);

        });

        btn.setOnAction(event -> {
            Alert alert = new Alert(AlertType.ERROR);
            if (name.getText().isEmpty() || email.getText().isEmpty() || pass.getText().isEmpty()
                    || rePass.getText().isEmpty()) {

                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Enter required fields.");
                alert.setX(800);
                alert.setY(50);
                alert.showAndWait();

            } else if (pass.getText().isEmpty() || pass.getText().length() < 6) {

                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Password is required and must be greater than or equals to 6 characters.");
                alert.setX(800);
                alert.setY(50);
                alert.showAndWait();
            } else if (pass.getText().length() >= 6 && !pass.getText().equals(rePass.getText())) {

                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Both password doesn't match.");
                alert.setX(800);
                alert.setY(50);
                alert.showAndWait();
            } else {
                System.out.println(name.getText());
                System.out.println(email.getText());
                System.out.println(pass.getText());
                System.out.println(rePass.getText());
                name.clear();
                email.clear();
                pass.clear();
                rePass.clear();
            }

        });
        // --------------------------------------------------------

        Label lb4 = new Label("Already have an account?");
        lb4.setFont(new Font(25));

        Button lg = new Button("Login");
        lg.setFont(new Font(25));
        lg.setStyle(
                "-fx-background-color: transparent; -fx-text-fill: orange; -fx-border-color: none; -fx-padding: 0 0 1 0;");

        lg.setOnMouseEntered(event -> {

            lg.setStyle(
                    "-fx-text-fill:red; -fx-background-color: transparent; -fx-border-color: none; -fx-padding: 0 0 1 0;");

        });

        lg.setOnMouseExited(event -> {

            lg.setStyle(
                    "-fx-text-fill:orange; -fx-background-color: transparent; -fx-border-color: none; -fx-padding: 0 0 1 0;");

        });

        HBox lhb = new HBox(40, lb4, lg);
        lhb.setStyle("-fx-padding:30");
        lhb.setAlignment(Pos.CENTER);

        // ------------------------------------------------------------------------------------------

        VBox right = new VBox(30, vb1, vb2, btn, lhb);
        right.setPrefWidth(700);
        right.setPrefHeight(700);
        right.setAlignment(Pos.CENTER);

        // --------------------------------------------------------------------------------------------

        HBox hbox = new HBox(100, left, right);
        hbox.setPrefWidth(1800);
        hbox.setPrefHeight(1000);
        hbox.setAlignment(Pos.CENTER);
        hbox.setStyle("-fx-padding: 80 80 0 20; ");

        // --------------------------------------------------------------------------------------------
        adminSignUpView.getChildren().add(hbox);
    }

    // return admin home page
    public Group getHomePage() {
        return adminHomePageView;
    }

    // return admin signup page
    public Group getSignUpPage() {
        return adminSignUpView;
    }

    // return adminlogin page
    public Group getLoginPage() {
        return adminLoginView;
    }

}
